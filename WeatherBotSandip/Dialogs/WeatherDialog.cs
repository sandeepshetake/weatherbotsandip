﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;

namespace BotApplication1.Dialogs
{
    [Serializable]
    public class WeatherDialog : IDialog<object>
    {
        public enum City
        {
            Pune,
            Mumbai,
            Kolhapur,
            Satara,
            Sangali,
        }
        public enum DayType
        {
            Today,
            Tomorrow,
        }
        protected string City_Name { get; set; }
        protected string Day_Type { get; set; }
        public Task StartAsync(IDialogContext context)
        {
            context.Wait(MessageReceivedAsync);

           return Task.CompletedTask;
        }

        private async Task MessageReceivedAsync(IDialogContext context, IAwaitable<IMessageActivity> argument)
        {
            await context.PostAsync("Wellcome To Weather System");
            PromptDialog.Choice(
              context: context,
              resume: MessageReceivedCityAsync,
              options: (IEnumerable<City>)Enum.GetValues(typeof(City)),
              prompt: "Hi.Enter City Name:",
              retry: "Selected City not avilabel . Please try again.",
              promptStyle: PromptStyle.Auto
              );
           
        }

        private async Task MessageReceivedCityAsync(IDialogContext context, IAwaitable<City> result)
        {
            City response = await result;
            this.City_Name = response.ToString();
            PromptDialog.Choice(
                         context: context,
                         resume: MessageReceivedDayAsync1,
                         options: (IEnumerable<DayType>)Enum.GetValues(typeof(DayType)),
                         prompt: "Hi.Enter which day weather you want:",
                         retry: "Selected day not avilabel . Please try again.",
                         promptStyle: PromptStyle.Auto
                         );
        }
        

        private async Task MessageReceivedDayAsync1(IDialogContext context, IAwaitable<DayType> result)
        {
            DayType response = await result;
            this.Day_Type = response.ToString();

            if (City_Name.ToLower().Equals("pune", StringComparison.InvariantCultureIgnoreCase) && Day_Type.ToLower().Equals("today", StringComparison.InvariantCultureIgnoreCase))
            {
                await context.PostAsync($"The {this.City_Name} city {this.Day_Type} Temprature is 25 c");
                context.Wait(MessageReceivedAsync);
            }
            else if (City_Name.ToLower().Equals("pune", StringComparison.InvariantCultureIgnoreCase) && Day_Type.ToLower().Equals("tomorrow", StringComparison.InvariantCultureIgnoreCase))
            {
                await context.PostAsync($"The {this.City_Name} city {this.Day_Type} Temprature is 26 c");
                context.Wait(MessageReceivedAsync);
            }
            else if (City_Name.ToLower().Equals("mumbai", StringComparison.InvariantCultureIgnoreCase) && Day_Type.ToLower().Equals("today", StringComparison.InvariantCultureIgnoreCase))
            {
                await context.PostAsync($"The {this.City_Name} city {this.Day_Type} Temprature is 27 c");
                context.Wait(MessageReceivedAsync);
            }
            else if (City_Name.ToLower().Equals("mumbai", StringComparison.InvariantCultureIgnoreCase) && Day_Type.ToLower().Equals("tomorrow", StringComparison.InvariantCultureIgnoreCase))
            {
                await context.PostAsync($"The {this.City_Name} city {this.Day_Type} Temprature is 28 c");
                context.Wait(MessageReceivedAsync);
            }
            else if (City_Name.ToLower().Equals("kolhapur", StringComparison.InvariantCultureIgnoreCase) && Day_Type.ToLower().Equals("today", StringComparison.InvariantCultureIgnoreCase))
            {
                await context.PostAsync($"The {this.City_Name} city {this.Day_Type} Temprature is 29 c");
                context.Wait(MessageReceivedAsync);
            }
            else if (City_Name.ToLower().Equals("kolhapur", StringComparison.InvariantCultureIgnoreCase) && Day_Type.ToLower().Equals("tomorrow", StringComparison.InvariantCultureIgnoreCase))
            {
                await context.PostAsync($"The {this.City_Name} city {this.Day_Type} Temprature is 30 c");
                context.Wait(MessageReceivedAsync);
            }
            else if (City_Name.ToLower().Equals("satara", StringComparison.InvariantCultureIgnoreCase) && Day_Type.ToLower().Equals("today", StringComparison.InvariantCultureIgnoreCase))
            {
                await context.PostAsync($"The {this.City_Name} city {this.Day_Type} Temprature is 31 c");
                context.Wait(MessageReceivedAsync);
            }
            else if (City_Name.ToLower().Equals("satara", StringComparison.InvariantCultureIgnoreCase) && Day_Type.ToLower().Equals("tomorrow", StringComparison.InvariantCultureIgnoreCase))
            {
                await context.PostAsync($"The {this.City_Name} city {this.Day_Type} Temprature is 32 c");
                context.Wait(MessageReceivedAsync);
            }
            else if (City_Name.ToLower().Equals("sangali", StringComparison.InvariantCultureIgnoreCase) && Day_Type.ToLower().Equals("today", StringComparison.InvariantCultureIgnoreCase))
            {
                await context.PostAsync($"The {this.City_Name} city {this.Day_Type} Temprature is 33 c");
                context.Wait(MessageReceivedAsync);
            }
            else if (City_Name.ToLower().Equals("sangali", StringComparison.InvariantCultureIgnoreCase) && Day_Type.ToLower().Equals("tomorrow", StringComparison.InvariantCultureIgnoreCase))
            {
                await context.PostAsync($"The {this.City_Name} city {this.Day_Type} Temprature is 34 c");
                context.Wait(MessageReceivedAsync);
            }
            else
            {
                await context.PostAsync("You entered wrong City Name Or Day");
                context.Wait(MessageReceivedAsync);
            }

        }
   
    }
}